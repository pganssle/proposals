# PyBay 2019 Proposal
## Build your Python Extensions with Rust!

#### Submitted by
Paul Ganssle

#### Themes
Scale & Performance

#### Audience Level
Intermediate

#### Talk Length
25 Minutes

### Brief Description
Looking to write compiled Python extensions, but don't want to worry about memory safety bugs or reference counting? You can write it in Rust! In this talk, I'll go into details about how to write Rust extension modules, and the trade-offs between different approaches you can take.

### Detailed Abstract
When your Python needs to be fast, one powerful tool is the ability to write compiled extension modules. Normally this is done using the C API, but that requires managing reference counts and memory allocation and is nearly impossible to do *correctly* for non-trivial programs. In this talk, we'll look at how you can write extension modules in Rust - a memory-safe systems programming language that is increasingly popular as a replacement for C and C++, and the [most loved language in the Stack Overflow developer survey 4 years running](https://insights.stackoverflow.com/survey/2019#technology-_-most-loved-dreaded-and-wanted-languages).

This talk will focus on `pyo3` and `milksnake`, two popular libraries that provide very different approaches for writing Rust extensions; it will cover a basic introduction to the libraries, and compare their relative strengths and weaknesses. These will also be compared to C and Cython extension modules from a performance, safety and maintainability perspective.

### Audience Take-Aways
The audience will learn the basics of how to use `pyo3` and `milksnake` to write Rust extensions, nd when it might be appropriate to use one or the other.

The talk assumes some *basic* familiarity with extension modules, and is aimed at Pythonistas who are interested in Rust and at people who currently write extension modules and are looking for a safer alternative to the C API. Familiarity with the syntax and basic concepts of Rust is helpful, but the talk should be understandable even for Rust beginners.

### Presentation experience / talk history
- I have not publicly given this talk before, but I have spoken at PyCon US,
  PyCon CA, PyBay 2017, PyGotham and others. Materials and videos of my
  previous talks can be found at https://ganssle.io/talks
- I am familiar with the internals of PyO3 because I wrote the `datetime`
  bindings for it, and have been involved with the development of the module
  since then.
- My writings on Python and other technical subjects can be found at my blog:
  https://blog.ganssle.io

### Links to previous talk content
- The slides for a 15-minute version of this talk
  [can be found here](https://pganssle-talks.github.io/rust-backends/#/)

### Speaker Bio
Paul Ganssle is a software developer, maintainer of python-dateutil and setuptools and contributor to many other open source projects. He lives in New York City and is interested in programming, languages, wearable electronics and sensors.

Expressions of opinion do not necessarily reflect the views of his employer.
