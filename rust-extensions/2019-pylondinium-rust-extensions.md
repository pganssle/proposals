# PyLondinium 2019 Proposal
## Build your Python Extensions with Rust!

#### Submitted by
Paul Ganssle

#### Duration
No preference

### Elevator Pitch
Looking to write compiled Python extensions, but don't want to worry about memory safety bugs or reference counting? You can write it in Rust! In this talk, I'll go into details about how to write Rust extension modules, and the trade-offs between different approaches you can take.

### Description
When your Python needs to be fast, one powerful tool is the ability to write compiled extension modules. Normally this is done using the C API, but that requires managing reference counts and memory allocation and is nearly impossible to do *correctly* for non-trivial programs. In this talk, we'll look at how you can write extension modules in Rust - a memory-safe systems programming language that is increasingly popular as a replacement for C and C++, and the [most loved language in the Stack Overflow developer survey 4 years running](https://insights.stackoverflow.com/survey/2019#technology-_-most-loved-dreaded-and-wanted-languages).

This talk will focus on `pyo3` and `milksnake`, two popular libraries that provide very different approaches for writing Rust extensions; it will cover a basic introduction to the libraries, and compare their relative strengths and weaknesses. These will also be compared to C and Cython extension modules from a performance, safety and maintainability perspective.

### Who and Why (Audience)
This talk assumes some *basic* familiarity with extension modules. It is aimed at Pythonistas who are interested in Rust and at people who currently write extension modules and are looking for a safer alternative to the C API. Familiarity with the syntax and basic concepts of Rust is helpful, but the talk should be understandable even for Rust beginners.

### Outline

(Note: Times at the highest level of the outline include times for the items on lower levels; times preceded by "T" are a running total)

- Introduction [1m 30s; T: 1m 30s]
    - Intro (45s)
    - Python as glue (45s)
- C API [3m; T: 4m 30s]
    - Pascal's Triangle: Python Version
    - C API Implementation
    - Performance benefits
        > Much faster than pure Python
    - Downsides
        > Buffer overflows, memory leaks, reference counting
- Rust: A basic overview [3m30s; T: 8m]
    - Introduction
    - Memory safety and concurrency
    - Borrowing and lifetimes
    - Traits and structs
- Rust API Bindings: PyO3 [3m; T: 11m]
    - Pascal's Triangle implementation + performance (1m)
    - How it works: FFI Layer (1m)
    - How it works: Safe Rust Layer (1m)
- PyO3: Making a module [3m30s; T: 14m30s]
    - Handling Python types (1m)
    - Handling exceptions (30s)
    - Building a class (2m)
- PyO3: Wrapper types and conversions [1m30s; T: 16m]
- FFI Bindings [3m; 19m]
    - Pascal's Triangle implementation + performance (1m 30s)
    - Building a basic module (1m 30s)
- FFI vs API [2m; T: 21m]
    > Pros and cons for each approach, with some possible use cases
    > where one approach or the other wins out.
- Parting thoughts [1m30s; 22m 30s]
    > Bring Cython into the race, and give an assessment of the relative
    > merits of Cython over Rust, and the situations where you'd want to
    > use one or the other

**Note**: I am using Pascal's Triangle as the demo project here, but that may
change before the final version. It will be something similar, though, exploring
one or two demo functions expressed in all the frameworks.


### Additional Notes
- I am familiar with the internals of PyO3 because I wrote the `datetime`
  bindings for it, and have been involved with the development of the module
  since then.
- The slides for a 15-minute version of this talk
  [can be found here](https://pganssle-talks.github.io/rust-backends/#/)
- I have not publicly given this talk before, but other talks I have given can
  be found at https://ganssle.io/talks
- My writings on Python and other technical subjects can be found at my blog:
  https://blog.ganssle.io

