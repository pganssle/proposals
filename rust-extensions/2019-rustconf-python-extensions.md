# RustConf 2019 Proposal
## Building Python Extensions with Rust

#### Submitted by
Paul Ganssle

#### Duration
Talk (30m)

### Abstract
Python is a popular, high-level programming language that is a famously good "glue" language, where it is common to write a high-level interface that drops into a compiled extension module for performance-sensitive code. Normally these compiled extensions are written in C, but whether you have an existing Rust library that you want to write Python bindings for or you'd like to avoid the manual memory management and reference counting you'd need to do with the C API, compiled Rust extensions are the way to go!

In this talk, I'll go into detail about the different approaches taken by `pyo3` and `milksnake`, two popular libraries that allow you to write Python extensions in Rust. In addition to a basic introduction to the libraries, I will compare their relative strengths and weaknesses, and how they compare to extension modules written in C and Cython from a performance, maintainability and safety perspective.

### Details
This talk assumes some *basic* familiarity with Python and extension modules. It is aimed at Pythonistas interested in using Rust in their Python modules as well as Rustaceans interested in writing Python bindings. Very little knowledge of Python is necessary to understand the talk.

**Outline**

(Note: Times at the highest level of the outline include times for the items on lower levels; times preceded by "T" are a running total)

- Introduction [2m 30s; T: 2m 30s]
    - Intro (1m)
    - Python as glue (45s)
    - Python bindings for Rust libraries (45s)
- C API [3m; T: 5m 30s]
    - Pascal's Triangle: Python Version
    - C API Implementation
    - Performance benefits
        > Much faster than pure Python
    - Downsides
        > Buffer overflows, memory leaks, reference counting
- Rust API Bindings: PyO3 [3m; T: 8m 30s]
    - Pascal's Triangle implementation + performance (1m)
    - How it works: FFI Layer (1m)
    - How it works: Safe Rust Layer (1m)
- PyO3: Making a module [3m 30s; T: 11m 30s]
    - Handling Python types (1m)
    - Handling exceptions (30s)
    - Building a class (2m)
- PyO3: Wrapper types and traits [2m 30s; T: 14m]
- FFI Bindings [3m; 17m]
    - Pascal's Triangle implementation + performance (1m 30s)
    - Building a basic module (1m 30s)
- Packaging and distribution [4m; T: 21m]
    - Intro to Python Packaging (2m 30s)
        - PyPI
        - `wheels` vs `sdists`
        - PEP 517: `pyproject.toml`
    - `setuptools-rust`, `pyo3-pack` and `milksnake` (1m 30s)
- FFI vs API [2m; T: 23m]
    > Pros and cons for each approach, with some possible use cases
    > where one approach or the other wins out.
- Parting thoughts [2m; 25m]
    > Bring Cython into the race, and give an assessment of the relative
    > merits of Cython over Rust, and the situations where you'd want to
    > use one or the other

**Note**: I am using Pascal's Triangle as the demo project here, but that may
change before the final version. It will be something similar, though, exploring
one or two demo functions expressed in all the frameworks.


### Pitch
There is a lot of overlap between the Rust and Python communities, and many
Python users are looking for more opportunities to use Rust. This talk is
intended to illustrate that Rust and Python are naturally complementary, and
to provide people a starting point for either writing Rust extensions for their
Python packages or writing Python bindings for their Rust crates.

**Additional information about me and the talk:**

- I am familiar with the internals of PyO3 because I wrote the `datetime`
  bindings for it, and have been involved with the development of the module
  since then.
- I am a maintainer of `setuptools` (the main library used to build Python
  packages), and thus have an understanding of some of the difficulties related
  to packaging.
- The slides for a 15-minute version of this talk
  [can be found here](https://pganssle-talks.github.io/rust-backends/#/)
- I have not publicly given this talk before, but other talks I have given can
  be found at https://ganssle.io/talks
- My writings on Python and other technical subjects can be found at my blog:
  https://blog.ganssle.io

