# PyCon 2020 Proposal
## Build your Python Extensions with Rust!

#### Submitted by
Paul Ganssle

#### Duration
30 minutes

### Description
When your Python needs to be fast, one powerful tool is the ability to write compiled extension modules. Normally this is done using the C API, but that requires managing reference counts and memory allocation and is nearly impossible to do *correctly* for non-trivial programs. In this talk, we'll look at how you can write extension modules in Rust - a memory-safe systems programming language that is increasingly popular as a replacement for C and C++, and the [most loved language in the Stack Overflow developer survey 4 years running](https://insights.stackoverflow.com/survey/2019#technology-_-most-loved-dreaded-and-wanted-languages).

This talk will focus on `pyo3` and `milksnake`, two popular libraries that provide very different approaches for writing Rust extensions; it will cover a basic introduction to the libraries, and compare their relative strengths and weaknesses. These will also be compared to C and Cython extension modules from a performance, safety and maintainability perspective.


### Who and Why (Audience)
The audience will learn the basics of how to use `pyo3` and `milksnake` to write Rust extensions, and when it might be appropriate to use one or the other.

The talk assumes some *basic* familiarity with extension modules; it is aimed at Pythonistas who are interested in Rust and at people who currently write extension modules and are looking for a safer alternative to the C API. Familiarity with the syntax and basic concepts of Rust is helpful, but the talk should be understandable even for Rust beginners.


### Outline
(Note: Times at the highest level of the outline include times for the items on lower levels; times preceded by "T" are a running total)

- Introduction [2m 30s; T: 2m 30s]
    - Intro (1m)
    - Python as glue (1m 30s)
- C API [3m 30s; T: 6m]
    - Sieve of Eratosthenes: Python Version
    - C API Implementation
    - Performance benefits
        > Much faster than pure Python
    - Downsides
        > Buffer overflows, memory leaks, reference counting
- Rust: A basic overview [3m 30s; T: 9m 30s]
    - Introduction
    - A Taste of Rust: resource ownership and its relationship to scope.
- Rust API Bindings: PyO3 [3m 15s; T: 12m 45s]
    - Sieve of Eratosthenes implementation + performance (1m 30s)
    - How it works: FFI Layer (1m)
    - How it works: Safe Rust Layer (45s)
- PyO3: Making a module [2m; T: 14m 45s]
    - Making a module (1m 15s)
        > Shows how to create a Python module with functions written in Rust
        > that return Python-specific types and can raise exceptions.
    - Making a class (45s)
- C FFI Bindings [3m 45s; 18m 30s]
    > Another approach to writing Rust API bindings is to expose a C FFI that
    > can be used by *any* programming language, and then use something like
    > milksnake and/or cffi to build bindings on the Python side.
    - Introduction to the C FFI approach and its benefits (2m 15s)
    - Using milksnake to write Python bindings for your C FFI 
        - Sieve of Eratosthenes performance (1m 30s)
- *Should* we use this? [4m 30s; T: 23m]
  - Cython for backends (1m)
  - Speed comparison chart (1m 15s)
      > I show a basic benchmark of the various approaches and then try to
      > caution everyone to not take the numbers *too* seriously - the result
      > is that, generally speaking, all the compiled extensions are the same
      > order of magnitude.
  - Pros and cons for PyO3 vs. C FFI (2m 15s)
- Opportunities for improvement [2m ; T: 25m]
  - FFI approach: helper libraries (1m)
      > A lot of the downsides of the C FFI approach could be improved with
      > some new crates providing ergonomic wrappers for exposing and using
      > C FFIs APIs.
  - PyO3: Get involved and contribute! (30s)
  - Final thoughts on where Rust and Python can or should go (30s)


## Additional Notes:
- I am familiar with the internals of PyO3 because I wrote the `datetime`
  bindings for it, and have been involved with the development of the module
  since then.
- Slides, videos and abstracts of other talks I have given can be found at
  https://ganssle.io/talks
- I gave this talk several times in 2019, at PyLondinium, EuroPython and
  PyGotham. Links to the videos and slides can be found at
  https://ganssle.io/talks/#python-backend-rust
- My writings on Python and other technical subjects can be found at my blog:
  https://blog.ganssle.io

