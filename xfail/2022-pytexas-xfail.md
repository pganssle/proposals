# PyTexas 2022 Proposal
## xfail and skip: What to do with tests you know will fail

#### Submitted by
Paul Ganssle

#### Track
Talks

### Elevator Pitch
What do you do with a test you know will fail? Skip it? Mark it as xfail? Or wait until the test is passing before adding it to your test suite?

In this talk, I'll explain `pytest`'s `skipif` and `xfail`, and dive into strategies for dealing with tests that will not pass.

### Description (in the schedule)
When you have a test that you know will fail, do you skip it, or mark it as an expected failure? Or should you wait until the test is passing before adding it to your test suite?

`pytest` offers two separate but distinct markers for tests that you know won't succeed: `skipif` and `xfail`. In this talk, I'll explore the differences between these two markers, and some strategies you can employ to make the best use of each. I'll also briefly touch on how to translate this approach into the `unittest` framework, since the underlying concepts are mostly transferable.

## Notes
### Experience
- I have extensive speaking experience, you can see a mostly full list of my previous talks (with videos) here: https://ganssle.io/talks/
- I have blog posts covering some of the topics I intend to cover (focused on `xfail`):
    1. [How and why I use `pytest`'s `xfail`](https://blog.ganssle.io/articles/2021/11/pytest-xfail.html)
    2. [A pseudo-TDD workflow using expected failures](https://blog.ganssle.io/articles/2021/11/pseudo-tdd-xfail.html)
    3. [`xfail` and code coverage](https://blog.ganssle.io/articles/2021/12/xfail-coverage.html)
- I was on the [Test & Code Podcast](https://testandcode.com) podcast, covering each of these blog posts. As of this writing, [the first](https://testandcode.com/171) and [second episodes](https://testandcode.com/174) have been released.
- This is a new talk, but I have given a rudimentary version of it [as a lightning talk](https://ganssle.io/talks/#xfail-lightning) at PyLondinium 2019 and PyGotham 2019. 


### Outline
Here is an outline providing the approximate contents of the talk. If a 45 minute talk is preferred, I can fairly easily add 15 more minutes of material.

(Note: Times at the highest level of the outline include times for the items on lower levels; times preceded by "T" are a running total)

- Introduction [ 1m 30s ]
- `skipif` and `xfail` [2m 30s; T: 4m]
    - Difference between `skipif` and `xfail` (1m)
    - When to use `skipif` (1m 30s)
- `xfail`: A worked example [5m 30s; T: 9m 30s]
    - Function `perfect_square` to calculate whether a number is a perfect square, with tests. (1m)
    - Finding a bug: doesn't work with negative numbers — add a failing test! (1m)
    - Fixing the bug: `XFAIL` becomes `XPASS` (30s)
    - Making failure to fail a failure: `strict=True` (1m 30s)
    - Why should you care? (1m 30s)
        > Document your acceptance criteria!
        > Test your tests!
        > Impose regression tests early!
- Bonus for pedants: Using `xfail` to simulate TDD [2m; T: 11m 30s]
    > Strategies for rewriting your VCS history with `git` and `hg` to make it
    > look like you used test-driven development.
- `xfail` and code coverage [4m; T: 15m 30s]
    - A `fizzbuzz` example with an `xfail` test (1m 15s)
    - Fixing the example: code coverage goes down! (45s)
    - Two strategies for adjusting your code coverage metrics to ignore `xfail`: (2m)
        - `nocover` with `pytest-cov`
        - Running `pytest` twice
- Using `xfail` and `skipif` [5m 30s; T: 21m]
    - Decorating with `pytest.mark.(xfail|skipif)` (30s)
    - Calling `pytest.skip` or `pytest.xfail` (1m 15s)
    - Using `xfail` and `skipif` with parameterized tests (1m 15s)
    - `pytest.mark.xfail(run=False)` and `--runxfail` (1m)
    - Some papercuts (e.g. use with `hypothesis` and `pytest-subtests`) (1m 30s)
- Marking expected failures in `unittest` [2m; T: 23m]
- Summary [2m; T: 25m]

## Bio
Paul Ganssle is a software developer, CPython core developer, maintainer of python-dateutil and setuptools and contributor to many other open source projects. He lives just outside of Hartford, CT with his family, where he guards an ancient treasure intended to be mankind's legacy.

Expressions of opinion do not necessarily reflect the views of his employer.
