# Gathering Related Functionality
## Patterns for Clean API Design

#### Submitted By
Paul Ganssle

#### Duration
No preference (25 minutes)

### Elevator pitch
This talk will arm you with some tools to design a library that "just works", but also has obvious escape hatches to handle corner cases. It covers several patterns for cleanly organizing related and overlapping functionality in a way that statisfies both humans and static analysis tools.

### Description
What do you do when you have to choose between designing your function for one of two common use cases?

How about when the same logical operations (say, multiplication or concatenation) need to have different implementations depending on the type of the arguments they are applied to?

These kinds of questions can be vexing when trying to design a clean, well-scoped API.

This talk will cover several strategies for grouping related functionality in a way that presents a logically clean interface to both humans and static analysis tools like type checkers and document generators.

This talk covers:

- Alternate constructors with @classmethod
- Inheritance with `@staticmethod` and `@classmethod`
- Dispatch by type
- A new convention for namespacing functions: ``variants``

### Outline

- Intro slide [30s]
- The problem to solve [ 1m30s, T: 2m ]
    - Introduce examples (45s)

        > Some common examples where the behavior implicitly
        > differs based on some argument or another

    - The problem for type checking (45s)

        > Show how confusion can occur when the dispatch
        > happens *inside* the function.

- Alternate constructors [ 4m, T: 6m ]
    - The Coordinate class (1m)

        > Represents coordinates in a latitude/longitude system

    - Constructor taking two floats or a string [1m30s]
        - Explaining the concept (30s)
        - Desired API vs. Actual API, type hinted (20s)
        - Errors not caught by type checker (25s)
    - Using an alternate constructor [1m30s]
        - Implementing ``from_str`` with the @classmethod decorator (45s)
        - Actual type-hinted API (15s)
        - Clearness of error messages (30s)
- classmethod vs. staticmethod [ 3m30s; T: 9m 30s ]
    - Alternate constructors (1m)

        > Show that static methods and class methods can both be
        > used to implement alternate constructors, but static
        > methods will *always* construct the base class or need
        > to be re-implemented in subclasses, while class methods
        > are heritable.

    - Abstract interfaces [1m 30s]
        - Static methods (30s)

            > Static methods can be used as an abstract interface on
            > the type when the implementation has no customizable
            > details

        - Class methods (1m)

            > Class methods can delegate some of the implementation
            > details to the class, allowing customization in subclasses

    - Static methods for namespacing (1m)

        > Show that methods can be placed on a class to namespace them
        > and indicate with syntax that they are related to the class,
        > even if they would work as free standing functions

- Functions [ 3m30s, T: 13m ] 
    - Introduction (30s)

        > Demonstrate a simple problem where multiple forms of a *function*
        > would be useful (e.g. printing text from a string, stream or file),
        > and show a naïve approach (print_text, print_text_from_stream).

    - variants solution (1m)

        > Implement ``print_text`` as a primary and two variant functions,
        > showing that they work differently, can have different function
        > signatures, and that they can call one another.

    - singledispatch solution (1m)

        > Implement ``print_text`` with ``functools.singledispatch``,
        > dispatching explicitly on type

    - variants + singledispatch (1m)

        > Show that both can be used together for explicit dispatch, and that
        > variants can allow duck typing on singledispatch-ed functions

- Example uses for function or method variants [ 6m; T: 19m ]
    - Variation in return type (1m)

        > Example of a function that returns a generator, but with an
        > eagerly calculated variant

    - Introducing a backwards-incompatible change (3m)

        > Demonstrate how changing the default behavior of a function or
        > method can be made easier by providing alternative explicit variants,
        > using Python's dict.items as an example

    - Caching and non-caching variants (1m)
    - Blocking and non-blocking variants (1m)

        > Show an async and a blocking variant of the same function

- General principles [ 5m; T: 24m ]
    - Utilize language semantics to mark relatedness (45s)
    - Clean top level APIs (1m)

        > Show that this works well from a tab completion and documentation
        > generation perspective.

    - Think about your type hints (1m15s)

        > Even if you aren't using type hints, you may want to rethink
        > your approach if you find it difficult to write down a typed
        > version of your signature.

    - Provide explicit versions of "magical" interfaces (2m)

        > Interfaces that "just work" are great in 90% of cases, but can
        > be very frustrating when you need a very specific behavior out
        > of them. The primary variant should do the right thing by default,
        > but specific functionality should be accessible.


### Additional notes
- I have extensive experience designing and working with open source library APIs as 
  the maintainer for python-dateutil and a maintainer of setuptools and a prolific
  contributor to many other open source libraries such as the CPython standard library,
  matplotlib and pandas.
- Previous talks I've given can be found at my website: https://ganssle.io/talks
- My writings on Python and open source can be found on my blog: https://blog.ganssle.io
