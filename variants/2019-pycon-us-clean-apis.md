# Gathering Related Functionality
## Patterns for Clean API Design

#### Submitted By
Paul Ganssle

#### Duration
30 minutes

### Description

What do you do when you have to choose between designing your function for one of two common use cases?

How about when the same logical operations (say, multiplication or concatenation) need to have different implementations depending on the type of the arguments they are applied to?

These kinds of questions can be vexing when trying to design a clean, well-scoped API.

This talk will cover several strategies for grouping related functionality in a way that presents a logically clean interface to both humans and static analysis tools like type checkers and document generators.

This talk covers:

- Alternate constructors with `@classmethod`
- Inheritance with `@staticmethod` and `@classmethod`
- Dispatch by type
- A new convention for namespacing functions: `variants`

### Who and Why (Audience)

This talk is aimed somewhere between beginner and intermediate programmers. It is understood that you would know things like class, constructor and namespace, but nearly all other concepts are explained as they are introduced.

Beginners will likely learn about new ways to think about interface design; for intermediate and advanced programmers, the talk is opinionated and makes a case for a specific kind of interface design, and the audience is encouraged to think it over and discuss whether or not it makes sense.

### Outline

- Intro slide [2m, T: 2m]

- General Principles [4m; T: 6m]

    - Related functionality should be grouped together (45s)
    - Interfaces should be discoverable (45s)
    - Think about function signatures (2m)
        > Look at functions where the type signature may give a misleading
        > picture of how the function operates, such as relativedelta's
        > constructor and a toy example where the return type depends on
        > a parameter's value.
    - Provide explicit versions of "magical" interfaces (30s)

- Example Class: Coordinate [2m45s; T: 8m45s]

    > Apply these general principles to a class representing Coordinates on
    > a globe, with longitude and latitude, created from two floats or a string

    - Initial approach (1m)
    - Comparing desired API with actual function signatures (1m)
    - Using an alternate constructor (45s)
        > Show how an alternate constructor gives something closer to the
        > desired, discoverable API

- Inheriting class behavior [2m45s; T: 11m30s]

    > Uses a Point class that can be constructed either from cartesian
    > or polar coordinates as a base class, and show the different ways
    > to reuse the base class and static methods in derived classes

    - Introduce the Point class (1m15s)
        > Defines an alternate constructor as a classmethod and implements
        > the cartesian->polar conversion in a staticmethod
    - Customizing behavior by overriding subclass classmethods (1m)
        > Create a NamedPoint class - show that the alternate constructor
        > mostly works "out of the box", but that overriding the classmethod
        > alone is enough to add the "construct with a label" functionality.
    - Customizing behavior by overriding subclass staticmethods (30s)
        > Create a Point3D class where overriding the staticmethod alone is
        > enough to add support for three dimensions.

- Functions [2m30s; T: 13m30s]

    > Show that the same problems we saw in object APIs apply to functions as well

    - Introduce a "print_text" function with a complicated function signature (1m30s)
        > Function tries to figure out if the thing you passed it is a path,
        > which it will open as text and print, or a file, which it reads and
        > prints, leading to complexity in the function signature, similar to
        > the relativedelta constructor example
    - dateutil.parse's complicated function signature (1m)
        > dateutil.parse is similar to the toy function, as the return type
        > depends on one of its input parameters

- variants [2m; T: 15m30s]
    > Introduce the concept of "function variants", called similar to static
    > methods on a function itself (e.g. print_text.from_stream), refactoring
    > the print_text function (with added functionality) using my variants
    > library (which adds syntactic sugar to make this pattern easier to
    > implement)

- Dispatch [2m15s; T: 17m45s]
    - Explicit dispatch (30s)
        > Show that function variants can be used to explicitly choose how
        > to interpret a parameter (e.g. is a string text, a URL or a path?)
    - Implicit dispatch with singledispatch (1m)
        > Implement the refactored print_text using singledispatch and wrapper
        > types for our "stringly-typed" parameters
    - Why not both? (45s)
        > Rework the example to allow both implicit and explicit dispatch

- Variation in return type [30s; T: 18m15s]

    > Show that function variants allow you to solve the "return type depends
    > on a parameter's value" problem presented earlier.

- Variation in caching behavior [30s; T: 19m45s]
- Variation in async behavior [30s; T: 19m15s]

- Syntactic marking of relatedness [1m; T: 20m15s]
    > Conclusion why it's a good idea to use the language syntax to mark
    > the relationships between functions / methods / objects, compared to
    > using a naming convention like an underscore
    - Sphinx extension: Documentation generators can understand syntax (30s)
    - Tab completion: Clean top-level APIS (30s)

- Conclusions [1m45s; 22m]
    > Discuss that this talk should be taken as an *argument for* these
    > principles, with examples in how to implement them, and that I encourage
    > everyone else to continue the conversation.

- Questions [5m; T: 27m]

These timings are based on the version I gave at PyGotham, which had a 20-minute
slot with 5 minutes for questions. The version at PyCon CA came in at ~25 minutes
with 5 minutes for questions, covering most of the same material but more
slowly.


### Additional notes
- I have extensive experience designing and working with open source library APIs as
  the maintainer for python-dateutil and a maintainer of setuptools and a prolific
  contributor to many other open source libraries such as the CPython standard
  library, matplotlib and pandas.
- Previous talks I've given can be found at my website: https://ganssle.io/talks
- I have given this talk twice before, at PyGotham and PyCon CA in late 2018; I
  also gave a lightning talk version of some of the "variants" content at PyCon
  2018. Videos and slides from these events can be found on "talks" section of
  my website (linked in the previous bullet).
- My writings on Python and open source can be found on my blog: https://blog.ganssle.io
