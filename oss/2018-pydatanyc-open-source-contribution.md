# Contributing to Open Source: A Guide

### Submitted by
Paul Ganssle

### Speaker Bio
Paul Ganssle is a software developer at Bloomberg and maintains several open source libraries, including `python-dateutil`. He lives in New York City and is interested in programming, languages, wearable electronics and sensors.

Expressions of opinion do not necessarily reflect the views of his employer.

## Brief Summary
Contributing to open source can be a rewarding experience, but many people don't know how, or find the process confusing or frustrating. This talk tries to bridge both the technical and social gap between new contributors and maintainers to help the listener have a positive open source experience.

## Description (Public)
Open source software powers a huge amount of the technology you use every day. Maintainers often exhort the public for contributions, but many developers find the process complicated or don't think that they have any skills they can contribute.

This talk will cover:

   - The different ways *you* can contribute to open source projects
   - The technical process of using GitHub for Issues and Pull Requests
   - How to maximize the chances that your contributions will be accepted

If you think it sounds cool to be able to make a meaningful contribution to software that touches millions of lives, but never thought you could - this talk is for you. If some open source software you use has a bug in it that just never seems to get fixed - this talk is for you. Even if you don't see the appeal of contributing to open source projects and just want to learn why you might want to do so - this talk is for you.

### Topic Overview
- Why Open Source?
- Types of contribution
- Basic GitHub workflow
- Raising a good issue
- Elements of an accepted pull request
- Choosing a first contribution
- Other ways to get involved

### Additional notes
- I am the sole maintainer of `python-dateutil`, which is a very widely-used
  open source project (though not an amazingly high-traffic project), and one
  of the maintainers of `setuptools`
- I have had pull requests for documentation, features and bug fixes accepted
  at *many* projects, including large projects like CPython, matplotlib and pandas.
- My writings on Python and open source can be found on [my blog](https://blog.ganssle.io)
- Previous talks I've given can be found at [my website](https://ganssle.io/talks)

(Note: This is a new talk so outline times are subject to change)

- Why Open Source? [ 2m, T: 2m]
    - Satisfaction
    - Agency
    - Reputation
- Types of contribution [ 3m30s, T: 5m30s ]
    - Issues (1m)
    - Bug fixes (1m)
    - Features (1m)
    - Documentation fixes (30s)
- Basic GitHub workflow [ 6m 30s, T: 12m ]
    - Basic git workflow (1m)

        > Clone a remote repository, make a branch, push to the remote

    - Making a pull request [4m]:
        - Three repository workflow (1m)

            > "local", "origin" and "upstream"

        - Forking the repo (15s)

            > Creates "origin" from "upstream"

        - Cloning the fork (15s)

            > Creates "local" from "origin"

        - Set the upstream (30s)

            > Connects "local" to "upstream"

        - Make a branch (1m)

            > Keeps your "master" clean
            > Makes it easier for maintainers to fix or clean up changes

        - Push the branch to origin (15s)
        - Make the pull request against upstream (45s)
    - Continuous integration (1m 30s)
        - How CI works (45s)

            > Most projects have continuous integration set up, which tests
            > against all supported environments. Let it run and then check it

        - Reading the CI failures (45s)

            > See what failed, see what the error was, try to fix it

- Raising a good issue [ 2m30s, T: 14m30s ]
    - Search for existing issues and PRs (15s)
    - Provide all your versions (update if possible) (15s)
    - Prepare a *minimal* working example (1m)
    - Provide a traceback if applicable (15s)
    - Clearly explain what the answer *is* and what it *should be* (15s)
    - Bonus: Do a little digging (30s)
- Elements of an accepted pull request [ 3m 30s, T: 18m00s ]
    - Introduction to the "maintainer psychology" (1m 30s)

        > Many or most projects are limited by maintainer time - anything that
        > requires extensive review will be put off. Maintainers need enough
        > context to work with but probably won't read a novel.
        > Strike a balance there.

    - Easy to verify - Tests, tests, tests! (45s)
    - Clear reasoning (30s)
    - Minimal support burden (30s)
    - Only make one change at a time (15s)
- Choosing a first contribution [ 5m 45s, T: 23m 45s ]
    - Pick a project (1m30s)

        > - Choose something that interests you.
        > - Look at how recently PRs have been *closed*
        > - Check if it has tests - even better, check if it has CI!
        > - Look at the license
        > - Read CONTRIBUTING.md

    - Find an issue and read the history (45s)

        > - Look for "good first issue" type labels
        > - Look for "help wanted"
        > - Avoid issues blocked by uncertainty about implementation 

    - Look for related issues (30s)

        > - May provide additional context
        > - May be able to close more than one issue at once

    - Dive into the code (1m)

        > - Find an entry point and follow the code
        > - Create a minimal working example and set a breakpoint
        > - Try to understand the larger context
        > - Create a failing test

    - Set up your build environment (1m 30s)
        - Check the documentation for build instructions (15s)
        - Work in a virtualenv (30s)
        - Make sure you can run the local test suite (45s)
    - Make a PR, and pay attention to the CI (15s)
    - Stick around! (15s)

        > Hit-and-run contributions are great, but there's a lot of fixed
        > cost. Fixing two issues is NOT twice as hard as fixing one!

- Other ways to get involved [ 1m, T: 24m 45s ]
    - Sprints (30s)

        > Stay after conferences for sprint events where everyone will work
        > together on open source programming. Often, the project maintainers
        > will be there and give real-time feedback.

    - Translate talks or uncertainty into documentation (15s)

        > Watch good talks about a project and try to use the library, keeping
        > track of anything not in the documentation. Then see if you can add
        > that information to the docs!

    - Help out in IRC or on Stack Overflow (15s)

        > Tracking down issues for others is often a good way to get a deeper
        > understanding of a project. Once you understand a project, you are
        > in a great position to contribute!

As I have not given this talk before, the timings above are more likely to
be too short than too long. A 40-minute talk would definitely include all
these topics, but a 25-30 minute talk will likely need to cut some.
