# PyCon US 2020 Proposal
## What to Do When the Bug Is in Someone Else's Code

#### Submitted by
Paul Ganssle

#### Duration
30 Minutes

### Description
It's generally better to use libraries than to write your own code, but what happens when you run into an issue that is correctly solved by modifying the *library code* rather than your own code? What if you need to deploy a fix *today*, but you can't count on the upstream library applying the required fixes and getting a new release through your deployment system before your deadline? This presentation will cover various stop-gap strategies (of varying desirability) for dealing with this situation, including:

- Working around the bug with wrapper functions
- Monkey patching the offending methods or functions
- Vendoring a patched version of the library into your application
- Maintaining a forked version in your local package manager


### Who and Why (Audience)
This talk is for a broad audience, and the techniques described do not rely on very deep knowledge of Python. Attendees can expect to learn some strategies for getting their code working while they work with upstream maintainers to resolve their issues while creating a minimum of future work for themselves.

Although these strategies are generally useful, this topic will be of particular interest to people who work in environments with shared deployments, such as employees at medium-to-large companies or people maintaining or distributing applications through a distribution (like Debian, Fedora or Arch Linux).


### Outline
(Note: Times at the highest level of the outline include times for the items on lower levels; times preceded by "T" are a running total)

- Introduction [ 5m ]
  - Speaker and talk intro (1m)
      > Disclaimer: This talk is organized as a series of strategic retreats
      > from doing the "right thing". By some measures, each strategy may be
      > considered worse than the last one, and it is for each person to
      > decide for themselves which hill to die on.
  - Establishing the problem (4m)
    - A bug in someone else's code (1m)
    - The Right Thing to Do™ (1m)
    - What can go wrong? (2m)
      - Production deadlines
      - Long release cycles (upstream)
      - Long deployment cycles (in house)

- Wrapper functions [ 5m; T: 10m]
  > Sometimes it is possible to write a wrapper function that solves your
  > problem. In most of this section, we'll cover the strategies for allowing
  > this wrapper function to become a thin alias when the bug is fixed upstream.
  - A wrapper function: When and Why? (1m)
  - Opportunistically upgrading: (4m)
      - By feature detection (1m 30s)
      - By explicit version (1m)
      - Runtime vs. compile-time (1m 30s)

- Monkey patching [ 5m; T: 15m]
  > Monkey patching is dynamically patching the library code by modifying the
  > class or module directly. It is fairly tightly scoped, but runs the risk of
  > creating incompatibilities if done incorrectly, and, since it modifies the
  > global namespace, may have unintended side-effects for other libraries
  > tested against the unpatched version.
  - Intro to monkey patching (1m)
  - Types of monkey patch (1m)
    > Cover monkey patching objects, functions and modules with some real
    > examples of each.
  - Scoping the patch correctly (1m)
    > Discuss how to localize the monkey patch as tightly as possible. May
    > also cover using a context manager to scope the patch in time as well.
  - Opportunistic upgrades (30s)
  - Downsides (1m 30s)

- Vendoring a patched copy of the library [5m; T: 20m]
  > Vendoring is the practice of including an entire copy of the library in
  > question into your application. Patching a vendored copy may be better than
  > deploying a patched version in your package manager or monkey patching
  > because a vendored package is tightly scoped to your package.
  - Intro (30s)
  - How to vendor a package (30s)
  - Strategies for maintaining the source code (2m)
    - Git submodule, git subtree or pulling in source with no history.
    - Pulling in upgrades
  - Opportunistically upgrading (30s)
  - Downsides (1m 30s)

- Maintaining and deploying a fork [ 3m; T: 22m ]
  > The last refuge should be deploying and maintaining a patched version of
  > the library (though for many people and at many companies it has always
  > been the first line of defense).
  - Introduction (30s)
  - Patch management (1m)
    > Note that many Linux distributions maintain some patches on top of the
    > software they distribute for various reasons, which is why they have
    > developed some tools and strategies to maintain patches.
    - Quilt
    - Maintaining a source control branch or fork
  - Downsides (1m 30s)

- Strategy recap [1m; 23m]
  > Summarize the pros and cons of each strategy.

- Final thoughts [ 5m; T: 29m ]
  > This is the part of the talk where I try to make the case that discretion
  > is the better part of valor, and that an important part of dealing wit this
  > sort of thing is knowing when *not* to deploy these strategies.
  - Patience is a virtue
    > Each of these strategies involves incurring some technical debt, and some
    > of them involve *imposing* that technical debt on everyone in your
    > organization. Consider developing strategies to allow you to patiently
    > wait for upstream fixes to deploy.
  - Strategies for getting your patches merged upstream
    > The best way to pay down your fork-related technical debt is to try to get
    > your patches upstream. This could be a full talk of its own, so I will
    > mostly give highlights and then pointers to full talks I and others have
    > given on this topic.
  - Success stories
    > Show a few examples where the maintenance burden has been improved by
    > these sorts of workflows.

This is an outline for a 30-minute version. I would be happy to expand it to a 45-minute version if desired. In doing so, I would probably work through more hands-on examples.

### Additional Notes
- I have spoken at many conferences, you can find my previous talks here: https://ganssle.io/talks/
- I am familiar with many shim and bridge-related solutions as a maintainer of several popular open source projects, including python-dateutil, setuptools and CPython. Working around bugs while maintaining compatibility with multiple Python and package versions gives one an opportunity to come up with many creative solutions to these kinds of problems.
- This is a new talk that I have not given publicly before, though I have spent time at my last two employers advising people dealing with exactly these issues using two different package management solutions.
