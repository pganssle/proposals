# PyCon US 2023 Proposal
## `pytest` for `unittest`ers

#### Submitted by
Paul Ganssle

#### Track
Talks

### Description
Are you a `unittest` user interested to learn more about `pytest`? Do you want to learn to write more idiomatic `pytest` tests? Do you use neither and want an overview of some of the differences between the two frameworks? If you answered yes to any of these questions, then this talk is for you!

Join us for an introduction to fixtures, test parameterization,and an explanation of some of `pytest`'s subtler user experience enhancements, in a talk that will just scratch the surface of `pytest`'s extensive feature set.

### Category
Testing

### Outline

- Introduction [1m; T: 1m]
    > `pytest` is extremely magical, which is a reason for skepticism, but in
    > practice it doesn't cause major problems, and `pytest` brings an enormous
    > feature set and a broad and deep ecosystem with it.
- Major differences from `unittest`: `assert` statements [4m; T: 5m]
    - Byte code rewriting (2m30s)
    - Advantage: No need for custom assert methods (45s)
    - Handling floats (45s)
- Improved error messages [2m; T: 7m]
    > `pytest` has some nice quality-of-life improvements for error messages,
    > including giving you context about what generated certain "magic numbers"
    > and adding a splash of color to draw your eye to salient features.
- `pytest` is compatible with unittest [30s; T: 7m30s]
    > `pytest` can be used to run your `unittest` tests, at the cost of some of
    > the features. It can also output data in a structured format similar to
    > unittest's. Both of these can ease the transition to `pytest` when you
    > have a lot of infrastructure built around `unittest`.
- Major differences from `unittest`: Classes [1m45s; T: 9m15s]
    - Classes are optional in `pytest` (45s)
        > Test cases are discovered based on naming conventions, not inheritance
        > Most setup/teardown done with fixtures
        > You can still use classes in `pytest`!
    - Classes in `pytest` (1m)
        > `pytest` can still be use classes if you want test case inheritance
        > or xUnit-style setup-teardown scopes. It also has some additional
        > xUnit-style scopes like function-level and module-level setup/teardown.
- Fixtures [4m45s; T: 14m]
    - Introduction (1m30s)
        > Fixtures allow for modular, composable code without mix-ins and 
        > complicated inheritance patterns. You define a fixture with a
        > specific name, then use it in a test function by giving that test
        > function a parameter *with that name* (this seems like a major source
        > of confusion for new users).
    - Fixtures are modular (1m)
        > Each fixture contains the setup and teardown code for the resource
        > or function it manages. This makes your code very modular, as you
        > only need to pay the setup and teardown costs for the specific
        > resources you use in any given test.
    - Fixtures are composable (1m)
        > Fixtures can request other fixtures, which allows you to build various
        > complex behaviors out of smaller parts using composition.
    - Fixture scopes (1m)
        > You can specify fixture scopes — function, class, module, package
        > or session.
    - Fixtures could be the subject of their own talk (15s)
        > This is only scratching the surface — there are many built-in
        > fixtures like `tmp_path` and `capsys`, it is possible to parameterize
        > fixtures, and a deep dive into the fixture search path is probably
        > worth doing if you are going to be using these extensively.
- Markers [2m15s; T: 16m15s]
    - Markers add metadata to a test (45s)
        > There are many built-in markers, and they can be a useful mechanism
        > for customization of behavior in plugins. They are also easily
        > extensible and can be used for filtering tests out of your test suite.
    - `xfail` and `skipif` (1m30s)
        > These are built-in markers for tests that don't pass. For a more
        > in-depth treatment of the topic, see my talk and/or blog posts on
        > the subject of `xfail`.
- Test parameterization [4m45s; T: 21m]
    - Using `pytest.mark.parametrized` (1m30s)
        > This is one of the most powerful techniques in the pytest arsenal,
        > and one that I use heavily. It allows you to follow some DRY strategies
        > without obfuscating layers of indirection.
    - Stacking `parametrize` decorators (1m)
        > In `pytest`, using more than one `parametrize` mark *stacks*, giving
        > you the cartesian product of the inputs. Use this judiciously, since
        > combinatorial explosions of test cases can cause slow test suites.
        >
        > If you find this slowing down your test suite interminably, consider
        > `hypothesis`.
    - Using `parameterized` or `googletest.parameterized` (45s)
        > These are testing tools that can allow for parameterization in
        > unittest-based frameworks.
    - Using subtests (`pytest` and `unittest`) (1m30s)
        > This is lightweight pseudo-parameterization, and has a place in both
        > `unittest` and `pytest`.
- Pytest configuration [4m; T: 25m]
    - Intro (1m30s)
        > Pytest is configurable in a lot of ways that `unittest` is not. In
        > some ways this is a double-edged sword, since various behaviors can
        > be configured in multiple ways and places, and it adds complexity to
        > the test suite. But in many ways it is useful for allowing projects
        > to customize their test suites to their use cases.
        >
        > pytest can be configured with command line flags, environment
        > variables, in `conftest.py`, and in one of the various config files.
    - Strict `xfail` (30s)
    - Filter warnings (30s)
    - `conftest.py` (1m30s)
- Plugins [1m15s; T: 26m15s]
    > One of the great things about `pytest` is that it has a large ecosystem
    > and a robust plugin mechanism. Highlights include `pytest-randomly`,
    > `pytest-cov`, `pytest-xdist` and `pytest-subtests`.
- Parting thoughts [2m; T: 28m15s]
    > A wrap up comparing `unittest` and `pytest`.


## Past Experience
- I have spoken at many conferences, you can find my previous talks here: https://ganssle.io/talks/
- I have extensive speaking experience, you can see a mostly full list of my previous talks (with videos) here: https://ganssle.io/talks/
- I have written many blog posts about testing, which can be found with the `testing` tag on my blog: https://blog.ganssle.io/tag/testing.html
- I have appeared on the Test & Code podcast several times: https://testandcode.com/guests/paul-ganssle
- In my open source work I extensively use `pytest`, but I also use `unittest` when contributing to CPython, and `googletest` at work, so I have a unique perspective as a "power user" of both frameworks.

## Have you previously given this talk before?
I gave a version of this talk internally at Google.
