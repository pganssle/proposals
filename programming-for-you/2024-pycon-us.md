# PyCon US 2024 Proposal
## Programming for Yourself

#### Submitted by
Paul Ganssle

#### Track
Talks

### Description

The career benefits of learning programming have become increasingly obvious over time, but few people talk about another great super-power you get from learning to program: the ability to make things that solve *your own problems*.

In this talk, I'll go through a few case studies from my own life wherein knowing how to program not only allowed me to solve problems for myself, but also allowed me to recognize that the problem I was having had a solution at all.

Additionally, I'll talk about ways the approach to engineering changes (and doesn't change!) when programming bespoke applications for a small audience.

### Category
Other

### Outline

- Introduction [45s; T:45s]
    > Who I am and why I'm talking to you
- Overview [3m30s; T:4m45s]
    - Overview (30s)
    - Bespoke applications can fit like a tailored suit (1m)
        > Off-the-rack clothing fits the average person, whereas tailored suits
        > are specially-made for you
    - It doesn't have to pay (1m)
        > You don't have to build something that will make money, or scale for
        > it to be valuable and / or worth your time.
    - Apps as home-cooked meals (1m30s)
        > In the essay [An app can be a home-cooked meal](https://www.robinsloan.com/notes/home-cooked-app/),
        > Robin Sloan suggests replacing "learn to program" with "learn to cook"
        > to see that there are a lot of ways you can use computers to bring
        > joy to yourself and others other than large scale commercial products.
        > Think of a bespoke application the way you'd think of a home-cooked
        > meal.

- Case study: Autotranscriber [3m15; T: 8m]
    - Identifying a problem (45s)
        > Introducing the problem: I make voice recordings to make notes about
        > my childrens' development, and I want to be able to look back on it
        > years later.
    - Identifying a solution (1m)
        > Sat on this idea for a while until I saw [Sumana Harihareswara's post on  Whisper](https://www.harihareswara.net/posts/2022/speech-to-text-with-whisper-how-i-use-it-why/)
        > which seemed to fit my use case perfectly.
    - Putting it together (1m30s)
        > Syncthing syncs my voice memos to a server, systemd services watch
        > the folder and triggers a Python script, which calls Whisper. The
        > whole thing is deployed in a docker container.
- Case study: Audiofeeder [6m30s; T: 14m]
    - audio-feeder (45s)
        > Application I built that turns audiobook files on my disk into a
        > directory of podcast feeds, so that I can intersperse my audiobooks
        > in with my podcast listening.
    - Taking a different approach: It doesn't have to scale (2m)
        - First version (1m)
            > A python 2 script that I ran to manually generate a .html file
            > and a bunch of .xml files. Lasted over a year.
        - Second version (1m45s)
            > A flask application that parses more information from the titles,
            > scrapes Google Books, and dynamically generates RSS feeds.
            >
            > It used a single large YAML file as a "database" because I wasn't
            > comfortable with SQL. This lasted 5 years.
    - Adding features that I wanted (3m)
        - User stories are easy when you're the only user (45s)
            > I was familiar with all the idiosyncracies of the app that annoyed
            > me, after several years of frequent use.
        - Improving the app (45s)
            > Deployed with a docker container, and migrated to a SQLite database.
        - My hyper-specific feature  (1m30s)
            > Added a nifty feature that uses ffmpeg to break up large files
            > along chapter boundaries and re-combine them in ~60m chunks. The
            > optimization algorithm is the first real life use case I've seen
            > for dynamic programming.
- Case study: Chord trainer [6m; T: 20m]
    - Chord trainer (1m30s)
        > Implementing a method for teaching perfect pitch to children from a
        > Japanese music school, to use with my 5-year-old son; this is a
        > single page application using HTML, CSS, JavaScript and Jekyll, most
        > of which I had very limited familiarity with before this project.
    - You can use software to connect to those around you (2m)
        > The method involves training a child on chords 5 times per day or more,
        > so this was in many ways a joint project between myself and my son.
        - Feedback from your loved ones is fun (45s)
            > My son loves being able to make feature requests, and I love being
            > able to surprise him with something new in the application.
        - My son showing off his inner hacker (45s)
            > When my son started doing chord trainer by himself, he also found
            > loopholes for cheating the system. It was actually really fun to
            > try and make the application cheating-proof, and made me proud
            > to see my son's inner hacker coming out.
        - Making videos (30s)
            > My son and I got to make videos that I posted on YouTube showing
            > people how to use the application, another fun project together.
    - You can learn a lot from building things (1m)
        > Building things is a great way to learn new skills. In this case I learned
        > a lot about JS, CSS, HTML and music theory. I learned how to make
        > installable progressive web applications. I feel much more confident
        > building front-end web applications now, and will feel comfortable
    - Bespoke applications can be useful to others (1m30s)
        > Turns out that another dad was doing the same method with his daughter
        > starting a bit before I started with my son, and he found and really
        > liked the application! He has since helped to design the implementation
        > for the next steps in the method.
        >
        > I've been on the other side of this as well —
        > [Project TBR](https://projecttbr.com) seems to be something that
        > someone built for themselves, but was actually something I've wanted
        > to build for myself for years.
- It doesn't have to be big [2m30s; T: 22m30s]
    - Anki decks (1m)
        > I have added little bits of JavaScript to make nice Anki cards for
        > myself and my son.
    - Gym calibration (45s)
        > I noticed that I seemed to be able to lift a *lot* more weight on
        > the cable machines at my gym than I would have expected, so I used
        > a luggage scale to find that they had a 2.2x leverage!
    - Python norm estimate (45s)
        > I was curious to know how easy it would be to estimate the RMS norm
        > of a podcast file without loading the whole thing into memory (if I
        > wanted to remaster it, so I took an example of a bunch of podcasts
        > and used different sampling methodologies to see which was most
        > efficient at getting closest to the true norm. Turns out randomly
        > sampling a podcast for less than 2 seconds' worth of points will give
        > you a very accurate estimate.
- Open Source Contributions for Yourself [1m30s; T: 24m]
    > While there are a lot of other considerations when contributing to open
    > source, the idea of building things for yourself is a very common
    > motivation for working on open source software.
    - Watch firmware (30s)
        > Sometimes I will carry a patch on some open source project, like I've
        > been doing with my PineTime watch software — mainly because there are
        > better open PRs covering the same feature there, but I still want the
        > feature.
    - AntennaPod Playback History (1m)
        > I was able to upstream a patch to my preferred podcast player
        > (AntennaPod) because it is open source. I wanted the playback history
        > to be unlimited rather than a fixed 40 items, and the patch was
        > accepted.
        >
        > Later, the knowledge I gained from this, and the knowledge of SQLite
        > from the audio-feeder project made it much easier to write a one-off
        > migration script moving all my podcasts to a new phone.
- Goes beyond programming [1m; 25m]
    > This approach goes beyond just software, it applies to pretty much any
    > productive skill. I was able to build a haptic compass into a hat to see
    > what that was like because of my knowledge of electronics. Similarly,
    > woodworkers are able to make custom furniture, and people who can sew can
    > make custom clothes and costumes.
    >
    > It is worth considering what other skills you have that you could use
    > to make bespoke products for yourself.
- Wrap up [2m; T: 27m]
    > I have been very concrete about this, so as we wrap up, I will try to draw
    > some more abstract takeaways for you.
    - Building for yourself is satisfying (30s)
    - Building for yourself is a great way to learn (30s)
    - Not everything has to scale (30s)
    - Sometimes the things you build for yourself do scale (30s)

## Have you given this talk before?
No, this would be the debut of this talk.
