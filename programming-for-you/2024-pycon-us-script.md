# Programming for Yourself

This is a rough script for the talk "Programming for Yourself". It started out
as an outline, but ended up getting quite detailed, so I thought I would break
it out into its own thing and simplify the outline.

## Script

- Introduction
    > Who I am and why I'm talking to you
- Overview
    > When people say, "learn to program", the case they make usually involves something
    > about the utility of software skills in the workforce, or the ability to build things
    > that scale, and it's true that the ability to distribute and copy software is one of
    > the most valuable things about it.
    >
    > This tends to overshadow software's other super power: it's extremely versatile
    > and there is often very little friction to create new things. When you can build
    > software for everyone, it turns out it's extremely easy to build software for *yourself*
    > as well! If you have a problem that can be solved with some software, it is often
    > surprisingly easy for software engineers to just *write that software*.

    - Bespoke applications can fit like a tailored suit
        > Off-the-rack clothing is designed to work pretty well for most people.
        > There's enough similarity in body shapes and sizes that you can usually
        > find something close to the style you want that basically fits you.
        >
        > If you are somewhat outside the norm of body shape or sizes, you will
        > probably find that off-the-rack clothes don't fit as well or look as good
        > without at least some alteration.
        >
        > The same goes for software — applications are written to match most
        > peoples' use cases most of the time. Most of the time that will be
        > sufficient, but you are unique, and chances are that you are someone's
        > edge case. As a software engineer, you basically have the software equivalent
        > of tailoring skills and an unlimited budget for cloth. You can build
        > *exactly* the application that you wish existed, one that fits you
        > like a perfectly tailored suit, even if it doesn't fit anyone else.

    - It doesn't have to pay
        > When I show people applications I've built, a lot of times they ask
        > whether I plan to sell it to make money, and the answer is almost
        > always no. I don't find anything wrong with that, but I have not
        > designed my applications to be a product. When designing a product,
        > you are looking for a problem that is niche enough that no one else
        > is offering good solutions, but also one that has a large enough
        > market to support you. Finding something like that is relatively hard,
        > but if you relax the constraint that your app has to pay for itself,
        > it turns out it's very easy to find problems in your life that you
        > can solve with software!

    - Apps as home-cooked meals
        > A few years ago I came across this article that provides an excellent
        > metaphor for the kind of software I've been building for ages:
        > [An app can be a home-cooked meal](https://www.robinsloan.com/notes/home-cooked-app/)
        >
        > In the essay, Robin Sloan compares bespoke apps to home-cooked meals.
        > People learning to cook will not be unsatisfied if they don't become
        > a professional chef, or write recipes that everyone uses. They are
        > often happy being able to understand what ingredients make the flavors
        > that they and their family will like, and knowing how to combine them
        > to make a high quality meal.
        >
        > We can approach software the same way: when you learn to program, you
        > can make your life a little better by building things that work for
        > you and the people you love.

- Case study: Autotranscriber
    > So far, I've talked in vague generalities, but now I'd like to get a bit
    > more concrete and show some of the software I've built just for myself,
    > or primarily for myself.

    - Identifying a problem
        > The first case study is something I call autotranscriber. In my opinion,
        > the software part of it is not that interesting, but I think it's a good
        > example of how building bespoke software has changed my approach to
        > problem solving.
        >
        > When my son was a toddler, I found that I wanted to know what my own
        > life was like at that time, and the information was either lost or
        > inaccessible, so I started making voice recordings about my son's progress
        > whenever I had a little time. After a few years, I wanted to refer
        > to them, but a dozen hours of audio is not exactly searchable. This
        > is the point where I thought, "could I solve this with software?"
    - Identifying a solution
        > I tried some text-to-speech solutions, but the high quality ones all
        > involved sending my data to the cloud, which I was not especially willing
        > to do.
        >
        > A few years into the project, I saw [Sumana Harihareswara's post on Whisper](https://www.harihareswara.net/posts/2022/speech-to-text-with-whisper-how-i-use-it-why/)
        > and it seemed like the perfect thing for my use case — I could run it
        > locally, and since I didn't need any kind of real-time speech-to-text,
        > I didn't even need a GPU.
    - Putting it together
        > The application itself is very simple. I've been making recordings on
        > my phone for years and using SyncThing to sync them to other computers.
        > Now I just needed to write a program to watch a directory for new recordings
        > and generate transcriptions from the ones that haven't already been transcribed.
        >
        > Watching a directory was easy enough with a systemd service, and so I wrote
        > a python script that searches a directory for audio files, checks to see
        > if a transcript exists in the output directory, and if not runs Whisper
        > to generate the transcript.
        >
        > Since then, I've found that I actually record voice memos about a lot
        > more things, knowing that the next day or so I'll have a reasonably accurate
        > transcript waiting for me (and, bonus, as speech-to-text improves over
        > time, I can go back and re-transcribe the existing files).
- Case study: Audiofeeder
    > I listen to a lot of podcasts and audiobooks, and I usually like to mix
    > up the content, since I find that I retain things better when they are
    > spread out rather than binged. So my playlist looks like ~1 hour of
    > podcasts, followed by ~1 hour of an audiobook, followed by ~1 hour of
    > podcasts, followed by ~1 hour of another audiobook. Unfortunately, I've
    > found that support for audiobook listening in podcast applications tends
    > to be weak, so I had the idea: maybe I can just host all my audiobooks
    > on a local server as podcasts. This is how I created the application
    > audio-feeder.
    - Taking a different approach: It doesn't have to scale
        > The first version of this was basically a single python script that
        > would I ran manually to scrape a directory and create an HTML page
        > that I served from nginx. It was the Python 2 days and I was not a
        > great programmer at the time, so at first it would break any time the
        > name of a book or an author contained unicode. This worked for me for
        > over a year.
        >
        > Eventually that became a big creaky, so I created a simple Flask
        > web-app. Even then, I had very limited time and at the time I didn't
        > know anything about SQL, so I just stored all the data in a big YAML
        > file (that way I could edit it manually!). The YAML file was loaded
        > from disk into a dictionary, which lasted for the lifetime of the
        > program. I had to re-start the program to load more books, but now
        > it had the ability to scrape descriptions and cover images from Google
        > books automatically. This system worked for over 5 years. I eventually
        > decided to upgrade to a sqlite database because the YAML file got so
        > big that it would take several minutes to load every time the application
        > booted up.
        >
        > I had built a system that would definitely not scale to everyone, was
        > only "secure" insofar as it had no ability to modify the contents of
        > my disk — and even then I was too scared to expose an endpoint to the
        > wider internet. But I had built something that worked for me for 5
        > years in just a few days. It didn't have to scale, yet.
    - Adding features that I wanted
        > After several years of dealing with the idiosyncracies of my bespoke
        > application, I knew all the things about it that annoyed me, and
        > critically I had become a much better programmer in the intervening time.
        > If this were a widely-used product, I would want to do user surveys
        > and gather telemetry to try and figure out which features could be
        > jettisoned and which features people wanted the most, but I was almost
        > certainly the only user of this software, so I knew exactly what
        > the majority of my users wanted. My user (me) wanted deployment to
        > be easier, wanted the database reloading to happen without restarting
        > the application, and for it to be faster. So I dockerized the
        > application, converted my YAML pseudo-database into a SQLite database
        > and added an endpoint that triggers updates from disk.
        - My hyper-specific feature
            > I also added a feature that was hyper-specific to my own use case,
            > but which has *really* made me happy. See, my application used to
            > just serve the audiobook files directly from my hard drive, one
            > "episode" per file, but this was very limiting, because a lot of
            > audiobooks are one big file, or are broken up into a million
            > 5-minute long files (one per chapter). This had me avoiding a lot
            > of audiobooks that are available but not in the form factor I
            > wanted.
            >
            > Luckily, most of the big files (like from Audible) have chapter
            > metadata, which makes for natural break-points, and the small
            > files are usually broken at natural break-points as well, so I
            > was able to add a wrapper around ffmpeg into my application that
            > extracts all chapter metadata from all the files, and then used
            > Python to find the optimal way to re-combine all the chapters into
            > files that are about an hour long.
            >
            > Interestingly, this is probably the only time that I've come across
            > a real-life use case for dynamic programming, since it's actually
            > very tricky to efficiently minimize the deviation from "one hour long"
            > when combining a number of files.
            >
            > This is an example of a feature that I'm extremely proud of, and
            > one that makes me happy every time I use it, but it's also something
            > that would almost certainly not be prioritized in a product aimed
            > at a larger audience. Even if I were contributing this feature
            > to an open source project, I imagine I'd get a lot of pushback,
            > since it added a dependency on ffmpeg, and a decent amount of
            > complexity. I also use it all the time, and I learned a lot of
            > interesting stuff in the process.
- Case study: Chord trainer
    > I have one more bespoke application to show you, and it's actually not
    > one written in Python at all. In mid-2023, when my son was 5 years old,
    > I came across a paper from a Japanese music school, claiming that they had
    > a method of teaching children between the ages of 2 and 6 to have perfect
    > pitch, by having the child master the ability to identify chords.
    >
    > My son was about to "age out" of the range that they claimed, and while
    > I was skeptical of their claims of being able to do this 100% of the time,
    > I figured I would try it and see how it went. I didn't have a piano, so
    > I went to an online piano and recorded the audio while I played the 9
    > chords from the first phase, then I built a dead simple web application
    > using hand-crafted HTML, Jekyll and JavaScript. I have only ever used
    > JavaScript out of necessity, and I would not consider myself a particularly
    > accomplished JavaScript programmer, but I got something working very
    > quickly.
    - You can use software to connect to those around you
        > The Eguchi method requires that you do 5 sessions of 25 identifications
        > every day. Each session is very short, but the requirement to do it
        > 5x/day tends to stop almost all busy parents.
        >
        > However, I was pretty motivated to get this working, and after a few
        > weeks my son started really getting into the swing of things. I added
        > a little kitty emoji that was happier when he did better, and he was
        > very motivated to concentrate to make the kitty happy.
        >
        > He also liked it when his friends came over and he was the "big man"
        > who could explain the rules of chord trainer to them. He has gotten
        > impressively good at it.
        - Feedback from your loved ones is fun
            > One thing about this process is that it's been a sort of project
            > that my son and I have been able to do together. At first, he
            > didn't know when a number was higher or lower than the target
            > number, and he wanted some feedback, so I made it so the number
            > turns green when you get past the target. Then when he started
            > getting perfect scores he didn't like how the numerator wasn't
            > green, so he insisted that I change that. Around Halloween, he
            > wanted me to change all the flags to pumpkins (which I didn't get
            > a chance to do, but I'm thinking of doing it for next year).
        - My son showing off his inner hacker
            > I also got a chance to see my son's inner hacker; at one particularly
            > busy part of the year, my son started doing Chord Trainer by himself,
            > and also he started getting a lot more perfect scores. At first I
            > was super impressed, but then I got suspicious that he always
            > wanted to do it by himself.
            >
            > Turns out, I had a bug (which I knew about but had not
            > prioritized fixing) where the number of correct identifications
            > was not per-level, so he would turn the chord trainer down to the
            > easiest level, get a perfect score, then turn it back up and
            > claim a 100% score.
            >
            > After I fixed this, he would go off by himself and just keep
            > resetting the level until he got a perfect score (because we
            > had a rule about needing 4 perfect scores in a row before going
            > on to the next level). I fixed this by adding a feature that
            > displays the history of attempts.
            >
            > Either way, it was fun having an adversarial collaborator on
            > the app, and I was extremely proud of my little hacker.
        - Making videos
            > We wanted other people (particularly those in my son's friend
            > group) to be able to use the application, so my son and I actually
            > made two instructional Youtube videos showing how to use it. This
            > was also a lot of fun, and contributes to the feeling that it's
            > a shared project.
    - You can learn a lot from building things
        > In both of these case studies, I had to learn a lot in order to implement
        > them. I didn't really know anything about JavaScript or music theory,
        > and my CSS knowledge was meager and out of date. This project has
        > given me a lot of confidence in fluency that I didn't have before.
        >
        > As time went on, I eventually wanted the app to be a little more
        > polished, and to work when offline, etc, and I found that I could
        > make it an installable progressive web app, which was surprisingly
        > easy, and works a lot better than I was expecting. Learning this feels
        > like it is going to make it a lot easier for me to make bespoke
        > GUI software for myself in the future.
    - Bespoke applications can be useful to others (see Project BTR)
        > I've spoken a lot about how building these things has been useful for
        > me, and indeed that is the main point here, but it's also true that
        > software *does* scale. Everything I've talked about here is actually
        > open-source. Audio-feeder can be installed with PyPI and there's a
        > docker-compose file you can use to deploy it. I specifically wanted
        > the chord trainer application to be super easy to deploy and fork,
        > and if you don't like the deployed version, you can just fork it on
        > github and enable Pages and your fork will be up and running.
        >
        > And in fact, it turns out that the chord trainer does have another
        > user! Despite the fact that I did zero marketing of it, someone
        > contacted me saying that he had actually been doing the Eguchi method
        > for 8 months (3 months before I even created the app)! And that his
        > daughter had reached the end of the portion of the method that I had
        > implemented. This was super gratifying to me, because while I built
        > the application for myself, it cost me nothing to make his and his
        > daughters' life a bit easier. And he has paid me back in many ways
        > by helping to develop the methodology for the next phase, and giving
        > feedback on the latest versions of the application.
        >
        > I've been on the other side of this as well, when I found
        > [Project TBR](https://projecttbr.com/), which takes your goodreads
        > to-read shelf and tells you which libraries have it in the format
        > you want. I've been meaning to build something like this for *ages*,
        > but never had time to do it; and the reason that the author put
        > forward was basically that they were scratching their own itch.
- It doesn't have to be big
    - Anki decks
        > I have added little bits of JavaScript to make nice Anki cards for
        > myself and my son.
    - Gym calibration
        > I noticed that I seemed to be able to lift a *lot* more weight on
        > the cable machines at my gym than I would have expected, so I used
        > a luggage scale to find that they had a 2.2x leverage!
    - Python norm estimate
        > I was curious to know how easy it would be to estimate the RMS norm
        > of a podcast file without loading the whole thing into memory (if I
        > wanted to remaster it, so I took an example of a bunch of podcasts
        > and used different sampling methodologies to see which was most
        > efficient at getting closest to the true norm. Turns out randomly
        > sampling a podcast for less than 2 seconds' worth of points will give
        > you a very accurate estimate.
- Open Source Contributions for Yourself
    > While there are a lot of other considerations when contributing to open
    > source, the idea of building things for yourself is a very common
    > motivation for working on open source software.
    - Watch firmware
        > Sometimes I will carry a patch on some open source project, like I've
        > been doing with my PineTime watch software — mainly because there are
        > better open PRs covering the same feature there, but I still want the
        > feature.
    - AntennaPod Playback History
        > Sometimes I'm lucky enough to be able to get my changes upstreamed;
        > for example, my preferred podcast player is AntennaPod, which has a
        > Playback History feature that previously kept a hard-coded limit of
        > 40, which was causing some problems for my workflow.
        >
        > I made a PR making this configurable, and then after feedback just
        > made it a lazy-loaded list with no limit. Another thing that took just
        > a little time and helped me immensely — I hope it helps others as well.
        >
        > As an aside, when transferring phones, I found that AntennaPod
        > wouldn't recognize all my downloaded podcasts, even though I had
        > transferred them over — because the database was storing absolute paths
        > or something. Because of the facility I gained with SQLite when
        > implementing audio-feeder and the knowledge of the SQL database that
        > AntennaPod uses, I was able to manually edit the SQLite backup database
        > to update the paths (using a Python script), saving myself a ton
        > of time and bandwidth.
- Goes beyond programming
    > So far I've been talking about programming, but the case can be made
    > that it applies to most productive skills. Before I was a programmer I
    > was a scientist, specializing in hardware, so I had some facility with
    > electronics and fabrication. With some minor electronics knowledge and a
    > 3D printer I added some hard-wired blinkers to my cargo bike. When I
    > wanted to know what it's like to have a "north sense" I built a little
    > prototype hat that acts as a haptic compass.
    >
    > How many people with woodworking skills have you seen just whip up some
    > little stand or piece of furniture to fit their needs perfectly?
    >
    > I am also very jealous of people who know how to sew well, since they can
    > make actual bespoke clothing for themselves.
    >
    > No matter what skills you have, it's worth taking a minute to think about
    > how you can make them work for you, even if it doesn't scale.
- Wrap up
    > I have been very concrete about this, so as we wrap up, I will try to draw
    > some more abstract takeaways for you.
    - Building for yourself is satisfying
    - Building for yourself is a great way to learn
    - Not everything has to scale
    - Sometimes the things you build for yourself do scale
