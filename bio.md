Paul Ganssle is a software developer at Google and contributor to various open source projects. Among other projects, he is a core developer of the Python language and created the zoneinfo module.

He lives in Somerville, MA with his wife and two children, where he also moonlights as a hunter tracking down the most dangerous game: dinosaurs.
