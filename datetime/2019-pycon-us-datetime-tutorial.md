# PyCon US 2019 Proposal
## Dealing with Datetimes

#### Submitted By
Paul Ganssle

## Description
Dealing with dates and times is famously complicated. In this tutorial, you'll work through a few common datetime-handling tasks and handle some edge cases you are likely to encounter at some point in your career.

This tutorial will cover:

- Working with time zones
- Serializing and deserializing datetimes
- Datetime arithmetic
- Scheduling recurring events

The format will be a mix of short lectures and hands-on exercises.

## Audience

This talk is aimed at people who already know Python and would like a more in-depth understanding of how to work with datetimes. You should be comfortable writing functions and classes and working with (installing, upgrading, importing) third party modules. You should expect to work through several datetime-related problems and gain a better understanding of some of the most common pitfalls in datetime-handling code.

## Outline

### General Outline

- Section 1: Overview [15m]

    This will be a general overview of the various complexities about dealing with datetimes,
    trying to touch on many edge cases to give people a general flavor of things to look out
    for. The main point of this will be to encourage people to try to carefully examine the
    assumptions they make about timekeeping.

- Section 2: Working with time zones [30m]

    This section will cover how time zones work in Python and go into depth about how to use
    them. It will be done mostly as a lecture with a few short interactive exercises mixed
    in. The material will primarily use `dateutil` as a time zone provider, but `pytz` will
    also be covered where its interface is different.

- Section 3: Serializing and deserializing datetimes [45m]

    This section will cover various ways to store and retrieve datetimes. It will examine
    writing and parsing a number of formats like logs and JSON, when and how to store time
    zones along with your datetimes. This will be a mix of lecture and exercises.

- Break [15m]

- Section 4: Calendar arithmetic [20m]

    This section will focus on calendar deltas and how to express otherwise ambiguous
    intervals like "until the end of the month" or "3 months from now" using `dateutil`'s
    `relativedelta` module.

- Section 5: Recurring events [50m]

    This section will cover how to express recurring events. It will largely be built around
    `dateutil`'s `rrule` module, and will be very hands-on. Together we will build a complex
    bus schedule with exceptions for holidays and one-off cancellations.

- Section 6: Conclusions and Q&A [10m]

    This will be a wrap-up of the various things that were covered, followed by a general
    format question and answer section.


### Detailed outline

#### Section 1: Overview [15m]

- Introduction [3m; T: 3m]
- Time zones: Edge cases [5m; T: 8m]
    - Daylight saving time
    - Non-integer offsets
    - Multiple DST transitions per year
    - Change of base offset without change in DST status
    - Change in DST status without change in offset
    - Missing and double days
    - Offset changes with little notice
    - Places where multiple time zones apply
    - Change in tzname without change in offset
    - Overlapping three-letter abbreviations
- Parsing [2m30s; T: 10m30s]
    - Complexity of ISO 8601 / RFC 3339
    - Variety of unambiguous but non-ISO ways people write dates
    - Ambiguous and context-dependent datetime strings
- Recurring events [2m30s; T: 13m]
    - Recurrences at fixed absolute intervals
    - At fixed calendar intervals... with DST
    - At uneven intervals:
        - On the Nth day of the month
        - During weekdays
        - On the 3rd Monday of the month
- Calendar offsets [2m; T: 15m]
    - 3 months after X
    - At the beginning of next month
    - At the *end* of next month

#### Section 2: Working with time zones [30m; T: 45m]

- Python's time zone model [5m; ST: 5m]
    - The `tzinfo` class [2m]
    - Exercise: Implement a UTC time zone class [3m]
- Getting a time zone aware datetime [5m; ST: 10m]
    - Creating an aware time zone (pytz, dateutil)
    - Converting from one zone to another (pytz, dateutil)
    - Exercise: Moving a datetime from LA to NYC
    - Exercise: Get the UTC time from an aware datetime
- Ambiguous and imaginary times [5m; ST: 15m]
    - Introducing the problem
    - `pytz`'s solution
    - PEP 495: The `fold` attribute, `dateutil`'s implementation
    - Exercise: Build a `pytz`-style exception-based localizer with `dateutil`
- Aware datetime semantics [10m; ST: 25m]
    - Inter-zone vs. intra-zone semantics
    - Calendar arithmetic vs absolute arithmetic
    - Exercise: Implement absolute_diff [2m]
    - Exercise: Implement relative_diff [3m]
- Handling local time [5m; ST: 30m]
    - Built-in behavior of `datetime`
    - `dateutil`'s `tzlocal` and `tzwinlocal`
    - Handling what happens if you change the time zone during the process

#### Section 3: Serializing and deserializing datetimes [45m; T: 1h30m]

- Introduction [5m; ST: 5m]
    - Prescriptive vs. descriptive parsing
    - Timestamps vs. datetimes
- Serializing and deserializing timestamps as integers [7m30s; ST: 12m30s]
    - Exercise: Write a function to dump a message with metadata to JSON
    - Exercise: Write a function to *read* the message with metadata
- Serializing and deserializing timestamps as strings [10m; ST: 22m30s]
    - `isoformat` and `fromisoformat`/`isoparse`
    - Exercise: Configure the logger to output timestamps in ISO 8601 format
    - Exercise: Write a log parser to retrieve log lines with times
- Parsing datetimes [7m30s; ST: 30m]
    - If you know but can't control the format
    - If you have a list of *possible* formats
    - If you don't know the format
    - Exercise: Parsing a datetime with three-letter time zone abbreviations
- Serializing datetimes with time zones [15m; ST: 45m]
    - Discussion to include how this is handled in different databases
    - Exercise: Write a JSON encoder and decoder hook to (de)serialize datetimes

#### Break [15m; T: 1h45m]
Give everyone a break to stretch their legs and whatnot.

#### Section 4: Calendar Arithmetic [15m; T: 2h00m]

- Introduction and `dateutil.relativedelta` [1m; ST: 1m]
- Absolute arguments (partial datetimes) [2m; ST: 3m]
    - Exercise: Try out a few deltas
        - March 3rd of this year
        - This date, but in 1951
        - Today, but at 12:15
- Relative arguments (calendar deltas) [2m; ST: 5m]
    - Exercise: Try out a few deltas
        - Six months from now (add to 2015-08-29, then 2016-08-29)
        - Two Wednesdays from now
- Combinations [7m; ST: 12m]
    - Expressing times relative to the month - beginning of the month
        - Exercise: Express the *end* of this month
    - Expressing days of a given year - mother's day (second Sunday in May)
        - Exercise: Implement a `tzinfo` for the current US rules
- Going beyond relativedelta [3m; ST: 15m]
    - Exercise: Next Monday, 9AM meeting

#### Section 5: Recurring events [50m; T: 2h50m]

- Introduction to `rrule` [7m30s; ST: 7m30s]
    - Basic `rrules` - dtstart, interval, frequency, count [5m]
    - Exercise: Play around with a few basic rules [2m30s]
- More advanced `rrule`s [10m; ST: 17m30s]
    - `by` rules: `byweekday`, `byyear`, etc [1m]
    - Designing efficient rrules [1m30s]
    - Exercise: Write an `rrule` to generate Martin Luther King day [5m]
    - Using the `skip` argument [1m]
    - Exercise: Write an `rrule` for "the end of the month" [1m30s]
- `rrulestr` - parsing and emitting RRULEs [2m30s; T: 20m]
- Using `rrules` with time zones [2m; T: 22m]
- Combining `rrules` with `rruleset` [25m; 47m]
    > This section will be a single exercise broken up into small chunks,
    > the attendees will design a bus schedule with at first simple, but later
    > complicated requirements
    - Combining two `rrules` with `rruleset.rrule` [7m30s]
        - Exercise: Implement a bus schedule with one frequency during the week
          and another on the weekends
    - Subtractive rules: `rruleset.exrule` [7m30s]
        - Exercise: Using `exrule` and `rrule`, change to modified service on
          Thanksgiving (fourth Thursday in November)
    - Removing one-off events with `rruleset.exdate` [5m]
        - Exercise: Remove all bus stops on a specific day with `rruleset.exdate`
    - Adding one-off events with `rruleset.rdate` [5m]
        - Exercise: Add back a small number of dates with `rrule.rdate`
- Using `rrule` with time zones [3m]

#### Section 6: Conclusions and Q&A [10m; T: 3h]
- Conclusions [5m]
- Q&A [5m]

## Additional Notes

I am the maintainer of python-dateutil and a frequent contributor to and reviewer of the datetime-related aspects of many projects such as pandas, matplotlib and CPython.

This is a new tutorial, though the material is adapted from several other talks and blog posts on closely-related subjects, notably:

- Working with Time Zones / Time Zone Troubles
    - PyLondinium 2018 (20m)
        - Video: https://youtu.be/TPrmyAZZPi0
        - Slides: https://pganssle.github.io/pylondinium-2018-timezones-talk
        - Repo: https://github.com/pganssle/pylondinium-2018-timezones-talk
    - PyBay 2017 (40m)
        - Video: https://www.youtube.com/watch?v=l4UCKCo9FWY
        - Slides: https://pganssle.github.io/pybay-2017-timezones-talk
        - Repo: https://github.com/pganssle/pybay-2017-timezones-talk

- python-dateutil: A delightful romp in the never-confusing world of dates and times
    - Taiwanese Data Professionals Meetup, 2018 (20m)
        - Video: https://youtu.be/EcG6Mg-4fHE?t=25m5s
        - Slides https://pganssle.github.io/tdp-2018-dateutil-talk
        - Repo: https://github.com/pganssle/tdp-2018-dateutil-talk
    - PyGotham 2016 (25m)
        - Video: https://www.youtube.com/watch?v=X_bCto95Imc
        - Slides: https://pganssle.github.io/pygotham-2016-dateutil-talk/
        - Repo: https://github.com/pganssle/pygotham-2016-dateutil-talk

A more complete listing of my talks can be found at my website: https://ganssle.io/talks/
Some of my writing can be found on my blog: https://blog.ganssle.io
