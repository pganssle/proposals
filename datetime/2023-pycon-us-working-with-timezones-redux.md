# PyCon US 2023 Proposal
## Working with Time Zones: Everything You Wish You Didn't Need to Know (zoneinfo edition)

#### Submitted by
Paul Ganssle

#### Track
Talks


### Description
Time zones are complicated, but they are a fact of engineering life. Time zones have [skipped entire days](http://www.bbc.com/news/world-asia-16351377) and repeated others. There are time zones that switch to [DST twice per year](https://www.timeanddate.com/time/zone/morocco/casablanca). But not necessarily every year.  In Python it's even possible to create datetimes with non-transitive equality (`a == b`, `b == c`, `a != c`).

In this talk you'll learn about Python's time zone model and other concepts critical to avoiding datetime troubles. Using the `zoneinfo` module introduced in Python 3.9 ([PEP 615](https://www.python.org/dev/peps/pep-0615/)), this talk covers how to deal with ambiguous and imaginary times, datetime arithmetic around a Daylight Savings Time transition, and datetime's new `fold` attribute, introduced in Python 3.6 ([PEP 495](https://www.python.org/dev/peps/pep-0495/)).

### Category
Python Language & Features

### Outline
(Note: Times at the highest level of the outline include times for the items on lower levels; times preceeded by "T" are a running total)

- Introduction [ 9m 30s ]
  - Speaker and talk intro (1m)
  - UTC (1m)
  - Time zones vs. offsets (1m 45s)
  - Fun examples (4m 15s)
    > Examples of counter-inuitive time zones, including:
    > - Change in DST without an offset change, change in offset without DST change
    > - Multiple yearly DST shifts
    > - Missing days, doubled days
  - Why do we need to work with time zones at all? (1m 30s)

- Python time zone model [ 1m 30s, T:11m ]
  - tzinfo objects, with example implementation (1m 30s)

- Ambiguous times [ 4m, T:15m ]
  - Introduction (30s)
  - PEP 495: Local Time Disambiguation  (1m 30s)
    > Introduction of the fold attribute, dateutil's backport
  - Comparing aware timezones (2m 30s)
    > Intra- vs. inter-zone semantics, a case of non-transitive datetime comparisons

- Imaginary times [ 2m 30s, T:17m 30s ]
  > Times that don't exist, why this led to non-transitivity in the previous case.

- Working with time zones [ 3m, T:20m30s ]
  - zoneinfo (1m)
    > How to attach, replace and convert between time zones.
  - pytz (1m 30s)
    - pytz's time zone model (1m 30s)
  - pytz-deprecation-shim (30s)
    - A shim for easing your users away from pytz.

- Handling ambiguous times [ 3m, T:23m30s ]
  > How to write code that responds when a wall time appears twice in a given zone
  - Overview (30s)
  - zoneinfo / dateutil (2m)
  - pytz (30s)

- Handling imaginary times [2m, T:25m30s ]
  > How to write code that won't generate non-existent datetimes
  - zoneinfo / dateutil (45s)
  - pytz (1m 15s)

- Conclusion [1m 30s, T:27m]

This is the outline for the 30-minute version. There is also a 45 minute version that expands on the types of time zone objects available, and goes into more detail about best practices such as storing and serializing timezone-aware datetimes and dealing with the IANA time zone database. In the 45 minute version, I will also discuss some of the pitfalls around local time, and in particular the problems that arise when your system time zone changes during the course of a program (*many* bets are off in that case).


### Additional Notes
- I am the maintainer for `python-dateutil` and I created the `zoneinfo` module in the standard library.
- I have spoken at many conferences, you can find my previous talks here: https://ganssle.io/talks/
- I gave a version of this talk at PyCon US 2019, prior to the introduction of the `zoneinfo` module:
    - https://ganssle.io/talks/#working-with-timezones-pycon
- I have given a 45-minute version of this talk at PyBay 2017 (though the content would be updated before the next time I give the talk): https://www.youtube.com/watch?v=l4UCKCo9FWY
- I have three blog posts on this subject as well, tagged "timezones" on my blog: https://blog.ganssle.io/tag/timezones.html
