# PyLondinium 2018 Proposal
## Working with Time Zones: Everything You Wish You Didn't Need to Know (Paul Ganssle)

#### Submitted by
Paul Ganssle

#### Duration
No preference

### Elevator Pitch
This talk is a deep dive into time zones, one of the most notoriously tricky concepts in programming. Every time I give some variation of this talk, someone tells me that something they learned made them go and fix time zone-related bugs that they had *in production*.

### Description
Time zones are complicated, but they are a fact of engineering life. Time zones have [skipped entire days](http://www.bbc.com/news/world-asia-16351377) and repeated others. There are time zones that switch to [DST twice per year](https://www.timeanddate.com/time/zone/morocco/casablanca). But not necessarily every year.  In Python it's even possible to create datetimes with non-transitive equality (`a == b`, `b == c`, `a != c`).

In this talk you'll learn about Python's time zone model and other concepts critical to avoiding datetime troubles. Using `dateutil` and `pytz` as examples, this talk covers how to deal with ambiguous and imaginary times, datetime arithmetic around a Daylight Saving Time transition, and datetime's new `fold` attribute, introduced in Python 3.6 ([PEP 495](https://www.python.org/dev/peps/pep-0495/)).

### Who and Why (Audience)
All users will learn a number of common time zone pitfalls in Python and in general. Beginners (and those unfamiliar with this particular corner of Python) will learn about the abstractions Python uses to represent time zones and why they are only *partially* compatible with pytz. Intermediate and advanced users will probably rush off in the middle of the talk to fix a bunch of their code that's going to break next time it gets called during a DST transition or deployed to a machine with a different time zone environment.

### Outline

(Note: Times at the highest level of the outline include times for the items on lower levels; times preceeded by "T" are a running total)

- Introduction [ 9m 30s ]
  - Speaker and talk intro (1m)
  - UTC (1m)
  - Time zones vs. offsets (1m 45s)
  - Fun examples (4m 15s)
    > Examples of counter-inuitive time zones, including:
    > - Change in DST without an offset change, change in offset without DST change
    > - Multiple yearly DST shifts
    > - Missing days, doubled days
  - Why do we need to work with time zones at all? (1m 30s)

- Python time zone model [ 1m 30s, T:11m ]
  - tzinfo objects, with example implementation (1m 30s)

- Ambiguous times [ 4m, T:15m ]
  - Introduction (30s)
  - PEP 495: Local Time Disambiguation  (1m 30s)
    > Introduction of the fold attribute, dateutil's backport
  - Comparing aware timezones (2m 30s)
    > Intra- vs. inter-zone semantics, a case of non-transitive datetime comparisons

- Imaginary times [ 2m 30s, T:17m 30s ]
  > Times that don't exist, why this led to non-transitivity in the previous case.

- Working with time zones [ 2m 30s, T:20m ]
  - dateutil (1m)
    > How to attach, replace and convert between time zones.
  - pytz (30s)
    - pytz's time zone model (1m 30s)

- Handling ambiguous times [ 3m, T:23m ]
  > How to write code that responds when a wall time appears twice in a given zone
  - Overview (30s)
  - dateutil (2m)
  - pytz (30s)

- Handling imaginary times [2m, T:25m ]
  > How to write code that won't generate non-existent datetimes
  - dateutil (45s)
  - pytz (1m 15s)

- Conclusion [1m 30s, T:26m 30s]

I'm assuming that a 30-minute slot does not include time for questions. If the talk portion should be closer to 30 minutes, I have 5 more minutes on tzinfo implementations provided by dateutil that I can add in.

### Additional Notes
- I know about this topic mostly because I am the maintainer for `python-dateutil`. I will try not to be biased against `pytz`
- I gave a 45-minute version of this talk (closer to 32 minutes talk time) at PyBay, [video here](https://www.youtube.com/watch?v=l4UCKCo9FWY), and [slides here](https://pganssle.github.io/pybay-2017-timezones-talk/#/).
- I gave a "lightning talk" version of this talk at PyCon 2017 in the morning session, [video here](https://youtu.be/SXl-pZnoaQ0?t=1225).
- I gave a more general talk on using python-dateutil at PyGotham 2016, [video here](http://pyvideo.org/pygotham-2016/python-dateutil-a-delightful-romp-in-the-never-confusing-world-of-dates-and-times.html)
(audio may be a bit messed up), [slides here](https://pganssle.github.io/pygotham-2016-dateutil-talk), [slide repo with Jupyter notebook](https://github.com/pganssle/pygotham-2016-dateutil-talk).
