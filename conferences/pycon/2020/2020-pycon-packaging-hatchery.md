# PyCon 2020 Hatchery Proposal: Python Packaging Summit

**Name**: Python Packaging Summit
**Organizing Team**: Python Packaging Authority
    - Paul Ganssle
    - Bernát Gábor

## Do you (or the team) have experience organizing conferences, workshops, tutorials, meetups?
We have organized unofficial Packaging summits during sprints at PyCon in 2018
and 2019.

Paul has been involved in organizing the PyData NYC meetup.
Bernát Gábor was involved in organizing the PyLondinium conference.
Both have organized and mentored at sprint events.

# Do you have experience putting together a talk program?
In addition to organizing the Packaging Summit in 2018 and 2019, Paul served on
the PyData NYC proposal selection committee in 2018 and 2019.


## Please describe the format of your proposed event.
We would like to hold a series of time-limited discussions on the most pressing
issues in Packaging and how to solve them. This year we would want to do either
a half-day or a whole-day event, the same day in parallel with the education
summit (specifically not in parallel to the Language Summit, since there is a
large degree of overlap between the participants of the Packaging summit and
the Language Summit).

For the event, we are planning on broadly continuing the same format as last
year, which is to select a certain number of topics in packaging, give a brief
introduction to the topic and the problems to be solved, and then open the
floor to a lightly moderated discussion about the issue for a fixed period of
time. One or more people will take notes, and at the end of each discussion
period, we will attempt to boil down the results of the conversation into
action items.

In previous years this was a fairly successful half-day event. Last year we had
success soliciting requests for topics in advance and choosing the topics for
discussion by online vote. We are planning to continue some variation on
this, where topics will be pre-selected; depending on whether the program expands
beyond a half-day event and how many "pressing topics" we have, we may allocate
some time for a open discussions, or reserve a certain number of slots for
discussions about topics proposed on the day, to allow for variation between
the voting audience and the attending audience, and to allow us to discuss
last-minute issues that come up.


## What subject are you proposing?*
Python Packaging


## What is your (or the team’s) experience/involvement with the subject?
We are members of the Python Packaging Authority, who maintain many of the
important tools involved in packaging Python (e.g. `pip`, `setuptools`,
`twine`, etc).


## Audience and participants
### Can you list some of the subject subtopics that will be addressed during your program?
We generally attempt to discuss topics that involve the intersection of
packaging projects, such as specifications, issues with interoperability and
the path forward. Here are some of the topics suggested and discussed from last
year's [topic suggestion
thread](https://discuss.python.org/t/packaging-mini-summit-pycon-us-2019-topic-suggestions/1534):

1. Future of editable installs
2. Recommendations around the `src/` layout (result: gained some consensus on
   recommending the `src/` layout more, and
   [`pypa/sampleproject`](https://github.com/pypa/sampleproject) has been
   updated to reflect this)
3. Adding a mechanism for allowing third parties to upload wheels (this
   discussion helped enable the creation of
   [conda-press](https://github.com/regro/conda-press))
4. How to expressing system dependencies
5. Fixing PyPI metadata (result: [PEP
   591](https://www.python.org/dev/peps/pep-0592/))

For more information, [detailed notes are available from the 2019 program](https://docs.google.com/document/d/1Wz2-ECkicJgAmQDxMFivWmU2ZunKvPZ2UfQ59zDGj7g/edit#heading=h.7rw2gk16okic).


### Do you have participants in mind for the track? Who are they?
We have previously had an open participation model, where anyone who shows up
may attend, but in our scheduling we've attempted to make it most convenient
for those involved in open source packaging tools, specifically PyPA, `conda`
and `conda-forge`, and linux distro packagers (e.g. Fedora, Debian). In
2019 we had at least [40 participants](https://docs.google.com/document/d/1Wz2-ECkicJgAmQDxMFivWmU2ZunKvPZ2UfQ59zDGj7g/edit#heading=h.sy5vfw8r78h),
which was approximately consistent with the event in 2018.

The goal is to take advantage of the fact that many stakeholders are present to
represent their perspectives, so that we can rapidly come to conclusions and
move forward.


## What will PyCon attendees gain from attending your program?
Those involved in packaging will get a chance to interact in person and quickly
iterate and coordinate on building the next improvements to Python packaging.

Those not currently involved or just getting involved will be able to get a
sense of what the pressing issues in Python packaging are, and hopefully get
involved (I believe we have a few people who got involved after attending this
program in the past).


## After Pycon
###  Will there be any actionable take away from the proposed program?
Yes, we generate a list of action items and often attempt to assign these to
individuals or working groups.


### How will you get Pythonistas more involved in the subject beyond the conference and how do you plan on proposing this take away to the attendees?
Many of our projects will be offering sprints, and often participants work on
some of the action items at the sprints.


## If applicable, how do you see this subject evolving into a spin-off event at PyCon going forward?
In a sense, going through the Hatchery is the next evolution of our event, that
has been taking shape over the last few years. We would like this to be an
ongoing and official event. We are not likely looking to "scale up" in the
sense of growing participation, since it is hard to scale discussions and
decision-making to large groups, but we are interested in improving the
efficiency of the process to allow for the maximum feasible participation.

I believe there are strong parallels in the Education and Language summits.

##  Aside from a room and basic audio-video needs (projector, microphone, podium), are there any additional requirements your event would have?
We have previously used markers and a "sticky easel pad" to take notes. A
sufficiently large whiteboard would also suffice.


## Is there anything else you'd like to share? 
Thank you for taking the time to evaluate this proposal.

